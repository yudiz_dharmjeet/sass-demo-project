import React, { useEffect, useState } from "react";
import Main from "./components/Main/Main";
import Navbar from "./components/Navbar/Navbar";

import "./App.scss";

function App() {
  const [news, setNews] = useState([]);
  const [category, setCategory] = useState("technology");
  const [loading, setLoading] = useState(true);

  function changeCategory(category) {
    setCategory(category);
  }

  // `https://newsapi.org/v2/top-headlines?country=in&category=${category}&apiKey=ae5cb5f8a3b149058e37271ba1be3857`
  async function getNews() {
    try {
      await fetch(
        `https://newsdata.io/api/1/news?apikey=pub_5096a1ef0ff72fdeccd1059233bde2c315ab&country=in&category=${category}`
      )
        .then((res) => res.json())
        .then((data) => {
          setNews(data.results);
          setLoading(false);
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (error) {
      throw error("Something went wrong in fetch api", error);
    }
  }

  useEffect(() => {
    getNews();

    return () => {
      setLoading(true);
    };
  }, [category]);

  console.log(news);

  return (
    <div className="app">
      <Navbar changeCategory={changeCategory} />
      {loading ? (
        <div className="loading_container">
          <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      ) : (
        <Main news={news} />
      )}
    </div>
  );
}

export default App;
