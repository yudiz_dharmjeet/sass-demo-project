import React from "react";
import PropTypes from "prop-types";

import "./Main.scss";
import NewsCard from "../NewsCard/NewsCard";

function Main({ news }) {
  return (
    <div>
      <div className="main">
        {news.map((newsItem, index) => {
          return <NewsCard newsItem={newsItem} key={index} />;
        })}
      </div>
    </div>
  );
}

Main.propTypes = {
  news: PropTypes.array,
};

export default Main;
