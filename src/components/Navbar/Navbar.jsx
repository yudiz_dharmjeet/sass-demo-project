import React from "react";
import PropTypes from "prop-types";

import "./Navbar.scss";

function Navbar({ changeCategory }) {
  return (
    <nav>
      <h4>NewsApp</h4>
      <ul>
        <li onClick={() => changeCategory("technology")}>Technology</li>
        <li onClick={() => changeCategory("business")}>Business</li>
        <li onClick={() => changeCategory("entertainment")}>Entertainment</li>
        <li onClick={() => changeCategory("science")}>Science</li>
        <li onClick={() => changeCategory("sports")}>Sports</li>
        <li onClick={() => changeCategory("health")}>Health</li>
      </ul>
    </nav>
  );
}

Navbar.propTypes = {
  changeCategory: PropTypes.func,
};

export default Navbar;
