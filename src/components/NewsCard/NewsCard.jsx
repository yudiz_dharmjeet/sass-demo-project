import React from "react";
import PropTypes from "prop-types";

import "./NewsCard.scss";

function NewsCard({ newsItem }) {
  let publishDate = new Date(newsItem.pubDate);

  return (
    <>
      {/* <div className="flip-card">
        <div className="flip-card-inner">
          <div className="flip-card-front">
            <img
              src={
                newsItem.urlToImage == null || newsItem.urlToImage == ""
                  ? "https://acadianakarate.com/wp-content/uploads/2017/04/default-image-620x600.jpg"
                  : newsItem.urlToImage
              }
              alt={newsItem.title}
            />
            <h3>{newsItem.title}</h3>
            <p>Published At: {publishDate.toLocaleString()}</p>
          </div>
          <div className="flip-card-back">
            <p>{newsItem.description}</p>
          </div>
        </div>
      </div> */}
      <div className="flip-card">
        <div className="flip-card-inner">
          <div className="flip-card-front">
            <img
              src={
                newsItem.image_url === null || newsItem.image_url === ""
                  ? "https://acadianakarate.com/wp-content/uploads/2017/04/default-image-620x600.jpg"
                  : newsItem.image_url
              }
              alt={newsItem.title}
            />
            <h3>{newsItem.title}</h3>
            <p>Published At: {publishDate.toLocaleString()}</p>
          </div>
          <div className="flip-card-back">
            <p>{newsItem.description}</p>
          </div>
        </div>
      </div>
    </>
  );
}

NewsCard.propTypes = {
  newsItem: PropTypes.object,
};

export default NewsCard;
